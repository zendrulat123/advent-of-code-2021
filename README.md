# advent of code 2021

My advent of code 2021 solutions

## Day 1 https://adventofcode.com/2021/day/1 
part 1 - https://go.dev/play/p/Wtf7Wv3Ubjc
8574  iterations          195376 ns/op nanoseconds             24 B/op          2 allocs/op  1.719s time/seconds

part 2 - https://go.dev/play/p/oBEkhIziTRe
4 iterations        260449500 ns/op  nanoseconds        35082 B/op       2944 allocs/op    1.292s time/seconds

## Day 2 https://adventofcode.com/2021/day/2
part 1 - https://go.dev/play/p/Ggj81cRbKyg
9104 iterations	  118018 ns/op nanoseconds	   48031 B/op	    2003 allocs/op    1.123s time/seconds


part 2 - https://go.dev/play/p/BkZcYc2FyWQ
2800  iterations        488325 ns/op  nanoseconds         48063 B/op       2002 allocs/op    1.449s time/seconds
